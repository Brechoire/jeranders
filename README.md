# Les tribulations de Jeranders

Les tribulations de Jeranders, est un site personnel où je m’amuse et m’entraîne sur le développement web. De plus, il me sert de pense-bête.


# Environnement de développement & outils

Le site est développé en PHP avec le framework [Symfony](https://symfony.com/).

**Environnement**
- PHP 8.0
- Apache 2.4.35
- MySQL 5.7.4
- Symfony 5.3.3
- Maildev

**Outils**

- [GitKraken](https://www.gitkraken.com/)
- [Cmder - Console Emulator](https://cmder.net/)
- [PhpStorm](https://www.jetbrains.com/fr-fr/phpstorm/)

## Setup

Téléchargement du repository avec git clone:

    git clone git@gitlab.com:Brechoire/jeranders.git
Installation des packages:

    composer install
Création du fichier .env.local à la racine du projet:

    cd Jeranders
    touch .env.local

Modifier le fichier `.env.local` :

    DATABASE_URL="mysql://root@127.0.0.1:3306/jeranders?serverVersion=5.7"

Ajout des fixtures:

`composer require --dev orm-fixtures`

Lancer les fixtures:

`php bin/console doctrine:fixtures:load`

Lancer le serveur web:

    php -S 127.0.0.1:8000 -t public

or

    symfony serve
Accéder au site [http://127.0.0.1:8000](http://127.0.0.1:8000)
