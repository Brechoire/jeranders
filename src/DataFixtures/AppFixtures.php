<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(
        private UserPasswordHasherInterface $passwordEncoder
    ) {
    }

    public function load(ObjectManager $manager): mixed
    {
        for ($i = 0; $i < 20; ++$i) {
            $user = new User();
            $user->setEmail('jeranders ' . $i . '@gmail.com');
            $user->setUsername('jeranders' . $i);
            $user->setPassword(
                $this->passwordEncoder->hashPassword(
                    $user,
                    'jerome'
                )
            );
            $manager->persist($user);
        }

        $manager->flush();
    }
}
